
package com.mitocode.mbean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.mitocode.model.Curso;

@Named 		  //Es manejada por JSF
@ViewScoped   // ambito
public class CursoBean implements Serializable {
	
	// Crear variable lista persona
	
	private List<Curso> lista;
	private Curso curso;
	
	//  Instanciar en el constructor
	public CursoBean() {
		lista = new ArrayList<>(); // metodo
		this.listar();			   // inicializaciòn
		
	}
	
	//Crear metodo
	public void listar() {
		
		for(int i= 0; i<100; i++) {
			// Crear objeto persona
			Curso tar = new Curso();
			
			// asignar valores en mmeoria
			tar.setIdCurso(i); 
			tar.setNombre("Curso ");
			tar.setHoras(2100);
			lista.add(tar);
		}
		
		
		
		// agregamos a la lista
		//lista.add(per);   // Hay que instanciar el constructor
		
		
	}

	public List<Curso> getLista() {
		return lista;
	}

	public void setLista(List<Curso> lista) {
		this.lista = lista;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	

}
