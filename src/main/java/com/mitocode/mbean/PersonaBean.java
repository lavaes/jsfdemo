package com.mitocode.mbean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.mitocode.model.Persona;

@Named 		  //Es manejada por JSF
@ViewScoped   // ambito
public class PersonaBean implements Serializable {
	
	// Crear variable lista persona
	
	private List<Persona> lista;
	private Persona persona;
	
	//  Instanciar en el constructor
	public PersonaBean() {
		lista = new ArrayList<>(); // metodo
		this.listar();			   // inicializaciòn
		
	}
	
	//Crear metodo
	public void listar() {
		
		for(int i= 0; i<100; i++) {
			// Crear objeto persona
			Persona per = new Persona();
			
			// asignar valores en mmeoria
			per.setIdPersona(i);
			per.setNombres("Luis");
			per.setApellidos("Vasquez");
			per.setEdad(39);
			lista.add(per);
		}
		
		
		
		// agregamos a la lista
		//lista.add(per);   // Hay que instanciar el constructor
		
		
	}
	
	public void enviar(Persona per) {
		this.persona = per;
	}
	

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public List<Persona> getLista() {
		return lista;
	}

	public void setLista(List<Persona> lista) {
		this.lista = lista;
	}

	
	
	

}
